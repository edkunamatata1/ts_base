/// <reference path="node.d.ts" />

// import http = require('http')
// var server = http.createServer(function(req, resp) {
//   resp.writeHead(200)
//   resp.end('Hello from Node.js')
// })
// server.listen(1500)
// console.log('Listening...')

class Car {
  engine: string
  constructor() {
    this.engine = engine
  }
  start() {
    alert(`Engine started: ${this.engine}`)
  }
  stop() {
    alert(`Engine stopped: ${this.engine}`)
  }
}

window.onload = function() {
  var car = new Car('V8')
  car.start()
  car.stop()
}
