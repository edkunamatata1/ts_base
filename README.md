### TS Base

Starter kit for cypress with typescript

## Get Started

1. `git clone https://edkunamatata1@bitbucket.org/edkunamatata1/ts_base.git`

2. `tsc filename.ts`

## Other info

# Deno Style Guide

- Use TypeScript
- Use the term "module" instead of "library" or "package"
- Do not use the filename index.ts nor index.js
- Within deno_std, do not depend on external code
- Within deno_std, minimize dependencies; do not make circular imports.
- For consistency, use underscores, not dashes in filenames.
- Format code using prettier.
- Exported functions: max 2 args, put the rest into an options object.
- TODO Comments
- Copyright headers
- Top level functions should not use arrow syntax
- Meta-programming is discouraged. Including the use of Proxy.
- If a filename starts with underscore, do not link to it: \_foo.ts
- Use JSDoc to document exported machinery
- Each module should come with tests
- Unit Tests should be explicit

### Todo
